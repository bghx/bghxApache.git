package com.bghx.apache;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import com.bghx.apache.Processor;

public class WebServer {

	public static void Socket(int port) {
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			while (true) {
				Socket socket = serverSocket.accept();
				Processor processor = new Processor(socket);
				processor.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws IOException {
		int port = 8888;
		if(args.length==1){
			port = Integer.parseInt(args[0]);
		}
		Socket(port);
		
	}	
}
